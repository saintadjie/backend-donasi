@extends('layouts.app', ['title' => 'Campaign - Admin'])

@section('content')
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-300">
    <div class="container mx-auto px-2 py-2">

        <section class="relative bg-blueGray-50">
            <header class="relative pb-6 mt-8">
                <div class="container mx-auto px-4">
                    <div class="flex items-center">
                        <a href="{{ route('admin.campaign.create') }}">
                            <div class="inline-flex items-center bg-white leading-none text-blue-600 rounded-full p-2 shadow text-teal text-sm">
                                <span class="inline-flex bg-blue-600 text-white rounded-full h-6 px-3 justify-center items-center">+</span>
                                <span class="inline-flex px-2">TAMBAH</span>
                            </div>
                        </a>

                        <div class="relative mx-4">
                            <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                                <svg class="h-5 w-5 text-gray-500" viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
                                        stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                            <form action="{{ route('admin.campaign.index') }}" method="GET">
                                <input class="form-input w-full rounded-lg pl-10 pr-4" type="text" name="q" value="{{ request()->query('q') }}" placeholder="Search">
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <div class="w-full mb-12 px-4">
                <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-indigo-100 text-black">
                    <div class="rounded-t mb-0 px-4 py-3 border-0 bg-blue-700">
                        <div class="flex flex-wrap items-center">
                            <div class="relative w-full px-4 max-w-full flex-grow flex-1 ">
                                <h3 class="font-semibold text-lg text-white">Campaigns</h3>
                            </div>
                        </div>
                    </div>
                    <div class="block w-full overflow-x-auto ">
                        <table class="items-center w-full bg-transparent border-collapse">
                            <thead>
                                <tr>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Judul Campaign</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Kategori</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Target Donasi</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Tanggal Berakhir</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse($campaigns as $campaign)
                                    <tr>
                                        <td class="border border-l-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">{{ $campaign->title }}</td>
                                        <td class="border border-gray-300 px-6 align-middle whitespace-nowrap p-4 text-center">{{ $campaign->category->name }}</td>
                                        <td class="border border-gray-300 px-6 align-middle whitespace-nowrap p-4 text-center">{{ moneyFormat($campaign->target_donation) }}</td>
                                        <td class="border border-gray-300 px-6 align-middle whitespace-nowrap p-4 text-center">{{ $campaign->max_date }}</td>
                                        <td class="border border-r-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">
                                            <center>
                                                <a href="{{ route('admin.campaign.edit', $campaign->id) }}" class="bg-indigo-600 px-4 py-2 rounded shadow-sm text-xs text-white focus:outline-none">EDIT</a>&nbsp;
                                                <button onClick="destroy(this.id)" id="{{ $campaign->id }}" class="bg-red-600 px-4 py-2 rounded shadow-sm text-xs text-white focus:outline-none">HAPUS</button>
                                            </center>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-white text-center p-3 rounded-sm shadow-md">
                
                                        <td class="px-5 py-2" colspan="3">
                                            Tidak ada data yang tersedia!
                                        </td>

                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @if ($campaigns->hasPages())
                            <div class="bg-white p-3">
                                {{ $campaigns->links('vendor.pagination.tailwind') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
                
        </section>
    </div>
</main>

<script>
    //ajax delete
    function destroy(id) {
        var id = id;
        var token = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'APAKAH KAMU YAKIN ?',
            text: "INGIN MENGHAPUS DATA INI!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'BATAL',
            confirmButtonText: 'YA, HAPUS!',
        }).then((result) => {
            if (result.isConfirmed) {
                //ajax delete
                jQuery.ajax({
                    url: `/admin/campaign/${id}`,
                    data: {
                        "id": id,
                        "_token": token
                    },
                    type: 'DELETE',
                    success: function (response) {
                        if (response.status == "success") {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL!',
                                text: 'DATA BERHASIL DIHAPUS!',
                                showConfirmButton: false,
                                timer: 3000
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'GAGAL!',
                                text: 'DATA GAGAL DIHAPUS!',
                                showConfirmButton: false,
                                timer: 3000
                            }).then(function () {
                                location.reload();
                            });
                        }
                    }
                });
            }
        })
    }
</script>
@endsection