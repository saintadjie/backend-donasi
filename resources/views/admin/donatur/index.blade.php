@extends('layouts.app', ['title' => 'Donatur - Admin'])

@section('content')
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-300">
    <div class="container mx-auto px-2 py-2">

        <section class="relative bg-blueGray-50">
            <header class="relative pb-6 mt-8">
                <div class="container mx-auto">
                    <div class="flex items-center">

                        <div class="relative mx-4">
                            <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                                <svg class="h-5 w-5 text-gray-500" viewBox="0 0 24 24" fill="none">
                                    <path
                                        d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
                                        stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                            <form action="{{ route('admin.donatur.index') }}" method="GET">
                                <input class="form-input w-full rounded-lg pl-10 pr-4" type="text" name="q" value="{{ request()->query('q') }}" placeholder="Search">
                            </form>
                        </div>
                    </div>
                </div>
            </header>
            <div class="w-full mb-12 px-4">
                <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-indigo-100 text-black">
                    <div class="rounded-t mb-0 px-4 py-3 border-0 bg-blue-700">
                        <div class="flex flex-wrap items-center">
                            <div class="relative w-full px-4 max-w-full flex-grow flex-1 ">
                                <h3 class="font-semibold text-lg text-white">Donatur</h3>
                            </div>
                        </div>
                    </div>
                    <div class="block w-full overflow-x-auto ">
                        <table class="items-center w-full bg-transparent border-collapse">
                            <thead>
                                <tr>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Nama Lengkap</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">E-mail</th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse($donaturs as $donatur)
                                    <tr>
                                        <td class="border border-l-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">{{ $donatur->name }}</td>
                                        <td class="border border-r-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">
                                            {{ $donatur->email }}
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-white text-center p-3 rounded-sm shadow-md">
                
                                        <td class="px-5 py-2" colspan="3">
                                            Tidak ada data yang tersedia!
                                        </td>

                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @if ($donaturs->hasPages())
                            <div class="bg-white p-3">
                                {{ $donaturs->links('vendor.pagination.tailwind') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
                
        </section>
    </div>
</main>

@endsection