@extends('layouts.app', ['title' => 'Slider - Admin'])

@section('content')
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-300">
    <div class="container mx-auto px-2 py-2">

        <section class="relative bg-blueGray-50">
            
            <div class="w-full mb-12 px-4">
                <br/>
                <div class="p-6 bg-white rounded-md shadow-md">
                    <h2 class="text-lg text-gray-700 font-semibold capitalize">UPLOAD SLIDER</h2>
                    <hr class="mt-4">
                    <form action="{{ route('admin.slider.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="grid grid-cols-1 gap-6 mt-4">
                            <div>
                                <label class="text-gray-700" for="image">GAMBAR</label>
                                <input class="form-input w-full mt-2 rounded-md bg-gray-200 focus:bg-white p-3" type="file" name="image">
                                @error('image')
                                    <div class="w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                                        <div class="px-4 py-2">
                                            <p class="text-gray-600 text-sm">{{ $message }}</p>
                                        </div>
                                    </div>
                                @enderror
                            </div>

                            <div>
                                <label class="text-gray-700" for="name">LINK SLIDER</label>
                                <input class="form-input w-full mt-2 rounded-md bg-gray-200 focus:bg-white" type="text" name="link" value="{{ old('link') }}" placeholder="Link Promo">
                                @error('link')
                                    <div class="w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                                        <div class="px-4 py-2">
                                            <p class="text-gray-600 text-sm">{{ $message }}</p>
                                        </div>
                                    </div>
                                @enderror
                            </div>

                        </div>

                        <div class="flex justify-start mt-4">
                            <button type="submit" class="px-4 py-2 bg-gray-600 text-gray-200 rounded-md hover:bg-gray-700 focus:outline-none focus:bg-gray-700">UPLOAD</button>
                        </div>
                    </form>
                </div>
                <br/>
                <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-indigo-100 text-black">
                    <div class="rounded-t mb-0 px-4 py-3 border-0 bg-blue-700">
                        <div class="flex flex-wrap items-center">
                            <div class="relative w-full px-4 max-w-full flex-grow flex-1 ">
                                <h3 class="font-semibold text-lg text-white">Slider</h3>
                            </div>
                        </div>
                    </div>
                    <div class="block w-full overflow-x-auto ">
                        <table class="items-center w-full bg-transparent border-collapse">
                            <thead>
                                <tr>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Gambar</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Link</th>
                                    <th class="px-6 align-middle border border-solid py-3 text-xs uppercase whitespace-nowrap font-semibold text-left bg-blue-800 text-white border-blue-700 text-center">Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse($sliders as $slider)
                                    <tr>
                                        <td class="border border-l-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">
                                            <center>
                                                <img src="{{ $slider->image }}" class="object-fit-cover rounded" style="width: 35%">
                                            </center>
                                        </td>
                                        <td class="border border-gray-300 px-6 align-middle whitespace-nowrap p-4">{{ $slider->link }}</td>
                                        <td class="border border-r-0 border-gray-300 px-6 align-middle whitespace-nowrap p-4">
                                            <center>
                                                <button onClick="destroy(this.id)" id="{{ $slider->id }}" class="bg-red-600 px-4 py-2 rounded shadow-sm text-xs text-white focus:outline-none">HAPUS</button>
                                            </center>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-white text-center p-3 rounded-sm shadow-md">
                
                                        <td class="px-5 py-2" colspan="3">
                                            Tidak ada data yang tersedia!
                                        </td>

                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @if ($sliders->hasPages())
                            <div class="bg-white p-3">
                                {{ $sliders->links('vendor.pagination.tailwind') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
                
        </section>
    </div>
</main>
<script>
    //ajax delete
    function destroy(id) {
        var id = id;
        var token = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'APAKAH KAMU YAKIN ?',
            text: "INGIN MENGHAPUS DATA INI!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'BATAL',
            confirmButtonText: 'YA, HAPUS!',
        }).then((result) => {
            if (result.isConfirmed) {
                //ajax delete
                jQuery.ajax({
                    url: `/admin/slider/${id}`,
                    data: {
                        "id": id,
                        "_token": token
                    },
                    type: 'DELETE',
                    success: function (response) {
                        if (response.status == "success") {
                            Swal.fire({
                                icon: 'success',
                                title: 'BERHASIL!',
                                text: 'DATA BERHASIL DIHAPUS!',
                                showConfirmButton: false,
                                timer: 3000
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'GAGAL!',
                                text: 'DATA GAGAL DIHAPUS!',
                                showConfirmButton: false,
                                timer: 3000
                            }).then(function () {
                                location.reload();
                            });
                        }
                    }
                });
            }
        })
    }
</script>
@endsection